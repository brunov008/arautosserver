var Sequelize = require('sequelize');
var env  = process.env.NODE_ENV || "development";
var path = require ('path');
var config = require(path.join(__dirname, '..', 'config', 'configAmbient.json'));

module.exports =  {
	pgConnect : function(){
		if (process.env.DATABASE_URL) {
			console.log('Ambiente de Producao')
			return new Sequelize(process.env.DATABASE_URL, config.production);	
		} else {      
			console.log('Ambiente de Desenvolvimento')
			return new Sequelize(config.development.database, config.development.username,
			 config.development.password, config.development); 
		}
	}
}