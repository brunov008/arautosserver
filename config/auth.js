var jwt = require("jwt-simple");
var moment = require('moment');

var params = {
  secretOrKey: 'B1234dsa2@3AH34PoSSs120U7',
};

module.exports = {

    tokengenerate: function(cpf){
      var expires = moment().add(1, 'day').valueOf();
      
      var payload = {iss : cpf, exp : expires};
      
      return jwt.encode(payload, params.secretOrKey);
    },

    tokenDecode: function(token){
      try{
        return jwt.decode(token, params.secretOrKey);
      }catch(err){
        return null;
      }
    },

    getTokenFromHeader(headers){
      if (headers && headers.authorization) {
        return headers.authorization; 
      } else {
        return null;
      }
    },

    isTokenValid(decoded){
      if (decoded.exp <= Date.now()) {
        return false;
      }
      return true;
    }
}