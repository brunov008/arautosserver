var nodemailer = require('nodemailer');
var path = require ('path');
var config = require(path.join(__dirname, '..', 'config', 'configAmbient.json'));

module.exports = {
	sendEmail : function(text, subject, listReceivers){
		var transporter = nodemailer.createTransport(config.email);

    	var mailOptions = {
		    from: 'arautosdonarguile@gmail.com', // sender address
		    to: listReceivers, // list of receivers
		    subject: subject, // Subject line
		    text: text
		};

		transporter.sendMail(mailOptions, function(error, info){
		    if(error){
		        console.log(error);
		        return false;
		    }else{		        
		        console.log(info.response);
		        return true;
		    };
		});
	}
}