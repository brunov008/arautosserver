var app = require('express')();
var express_load = require('express-load');
var express = require('express');
var expressValidator = require('express-validator');
var bodyParser = require('body-parser');

app.set('port', (process.env.PORT || 8080));

app.set('view engine', 'ejs');

app.set('views', './app/views');

app.use(express.static('./app/public'));

app.use(bodyParser.json());

app.use(expressValidator());

app.use(bodyParser.urlencoded({
  extended: true
}));

express_load('app/routes')
	.then('config/connectiondb.js')
	.then('config/auth.js')
	.then('config/email.js')
	.then('app/models')
	.then('app/controllers')
	.into(app);

module.exports = app;
