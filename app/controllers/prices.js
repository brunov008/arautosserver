var tokenJSON, decoded, isTokenValid;

module.exports.listar = function(application, request, response){
	
	token = application.config.auth.getTokenFromHeader(request.headers);

        if (token != null){

            decoded = application.config.auth.tokenDecode(token);

            if (decoded != null) {

                isTokenValid = application.config.auth.isTokenValid(decoded);

                if (isTokenValid) {

                    tokenJSON = {
                    			response:
                    				{
                    				status : 'OK', 
                    				mensagemRetorno : 'Lista Gerada com Sucesso.',
                                    retorno : 
                                    	[
                                    		{
                                    			price : "R$60,00",
                                    			description : "Combo de 1 hora com 1 narguile"
                                    		},

                                    		{
                                    			price : "R$400,00",
                                    			description : "Combo de 1 hora com 5 narguiles"
                                    		},

                                    		{
                                    			price : "R$120,00",
                                    			description : "Combo de 2 horas com 1 narguile"
                                    		}
                                    	]
                    				}
                    			}

                    return response.json(tokenJSON);
                } else {
                    tokenJSON = {
                        response :
                            {
                            status : 'Error',
                            mensagemRetorno : 'Acesso Expirado, faça login novamente'
                            }
                        }
                    return response.json(tokenJSON);
                }
            } else {
                tokenJSON = {
                        response :
                            {
                            status : 'Error',
                            retorno : 'Token Invalido'
                            }
                        }
                return response.status(400).json(tokenJSON);
            }
        } else{
            tokenJSON = {
                        response : 
                            {
                            status : 'Error',
                            retorno : 'Authentication Failed'
                            }
                        }
          return response.status(400).json(tokenJSON);
    }
}