var tokenJSON, decoded, isTokenValid;

module.exports = {

    cardtoken : function(application, req, res){
        
        token = application.config.auth.getTokenFromHeader(req.headers);

        if (token != null){

            decoded = application.config.auth.tokenDecode(token);

            if (decoded != null) {

                isTokenValid = application.config.auth.isTokenValid(decoded);

                if (isTokenValid) {

                    tokenJSON = {
                    			response:
                    				{
                    				status : 'OK', 
                    				retorno : 'Token e email Gerado com Sucesso.', 
                    				tokenCliente : '386083A38D9A40D5B0CFC56F1206CDD2',
                                    emailCliente : 'sacakurajonnathan@gmail.com'
                    				}
                    			}

                    return res.json(tokenJSON);
                } else {
                    tokenJSON = {
                        response :
                            {
                            status : 'Error',
                            mensagemRetorno : 'Acesso Expirado, faça login novamente'
                            }
                        }
                    return res.json(tokenJSON);
                }
            } else {
                tokenJSON = {
                        response :
                            {
                            status : 'Error',
                            retorno : 'Token Invalido'
                            }
                        }
                return res.status(400).json(tokenJSON);
            }
        } else{
            tokenJSON = {
                        response : 
                            {
                            status : 'Error',
                            retorno : 'Authentication Failed'
                            }
                        }
          return res.status(400).json(tokenJSON);
        }
    },

    cardcustomercreate : function(application, req, res){

        req.check('cardholder', 'Campo cardholder obrigatório').notEmpty();
        req.check('cardNumber', 'Campo cardNumber obrigatório').notEmpty();
        req.check('cardExpiry', 'Campo cardExpiry obrigatório').notEmpty();
        req.check('cardCvv', 'Campo cardCvv obrigatório').notEmpty();
        req.check('street', 'Campo street obrigatório').notEmpty();
        req.check('district', 'Campo district obrigatório').notEmpty();
        req.check('city', 'Campo city obrigatório').notEmpty();
        req.check('state', 'Campo state obrigatório').notEmpty();
        req.check('postalCode', 'Campo postalCode obrigatório').notEmpty();

        //PODE RETORNAR NULO (opcional)
        /*req.check('complement', 'Campo complement obrigatório').notEmpty();
        req.check('addressNumber', 'Campo addressNumber obrigatório').notEmpty();*/

        validate = req.validationErrors();

        if (validate) {
            tokenJSON = {
                            response : {
                                status : "Error",
                                retorno : validate[0]['msg']
                            }
                        }
            return res.json(tokenJSON); 
        }

        token = application.config.auth.getTokenFromHeader(req.headers);

        if (token != null) {

            decoded = application.config.auth.tokenDecode(token);

            if (decoded != null) {

                isTokenValid = application.config.auth.isTokenValid(decoded);

                if (isTokenValid) {

                    CreditDAO = application.app.models.creditDAO;

                    CreditDAO.findOne({
                            where : {idCard : 1}
                        })
                        .then(() => {
                                CreditDAO.update(
                            {
                                cpf : decoded.iss,
                                cardholder : req.body.cardholder,
                                cardNumber : req.body.cardNumber,
                                cardExpiry : req.body.cardExpiry,
                                cardCvv : req.body.cardCvv,
                                street : req.body.street,
                                district : req.body.district,
                                city : req.body.city,
                                state : req.body.state,
                                complement : req.body.complement,
                                postalCode : req.body.postalCode,
                                addressNumber : req.body.addressNumber
                            },
                            {where : {cpf : decoded.iss}
                        })
                        .then(() => {

                                tokenJSON = {
                                                response : {
                                                    status : "OK",
                                                    hasAccount : true,
                                                    mensagemRetorno : "Cartão " + req.body.cardNumber + " atualizado com sucesso",
                                                    retorno : {
                                                        cardholder : req.body.cardholder,
                                                        cardNumber : req.body.cardNumber,
                                                        cardExpiry : req.body.cardExpiry,
                                                        cardCvv : req.body.cardCvv,
                                                        street : req.body.street,
                                                        district : req.body.district,
                                                        city : req.body.city,
                                                        state : req.body.state,
                                                        complement : req.body.complement,
                                                        postalCode : req.body.postalCode,
                                                        addressNumber : req.body.addressNumber
                                                    }
                                                }
                                            }
                                return res.json(tokenJSON);
                            })
                            .catch(err => {
                                tokenJSON = {
                                            response : 
                                                {
                                                status : 'Server Error',
                                                mensagemRetorno : err
                                                }
                                            }
                                return res.status(500).json(tokenJSON)
                            })
                        })
                        .catch(() => {
                            CreditDAO.sync().
                            then(() => {

                                return CreditDAO.create(    
                                    { 
                                        cpf : decoded.iss,
                                        cardholder : req.body.cardholder,
                                        cardNumber : req.body.cardNumber,
                                        cardExpiry : req.body.cardExpiry,
                                        cardCvv : req.body.cardCvv,
                                        street : req.body.street,
                                        district : req.body.district,
                                        city : req.body.city,
                                        state : req.body.state,
                                        complement : req.body.complement,
                                        postalCode : req.body.postalCode,
                                        addressNumber : req.body.addressNumber
                                    }
                                ).then( card => {
         
                                    tokenJSON = {
                                                response : {
                                                    status : "OK",
                                                    hasAccount : true,
                                                    mensagemRetorno : "Cadastro do cartão " + req.body.cardNumber + " efetuado com sucesso",
                                                    retorno : {
                                                        cardholder : card.dataValues.cardholder,
                                                        cardNumber : card.dataValues.cardNumber,
                                                        cardExpiry : card.dataValues.cardExpiry,
                                                        cardCvv : card.dataValues.cardCvv,
                                                        street : card.dataValues.street,
                                                        district : card.dataValues.district,
                                                        city : card.dataValues.city,
                                                        state : card.dataValues.state,
                                                        complement : card.dataValues.complement,
                                                        postalCode : card.dataValues.postalCode,
                                                        addressNumber : card.dataValues.addressNumber
                                                    }
                                                }
                                            }
                                        return res.json(tokenJSON);
                                    });
                            })
                            .catch(err => {

                                tokenJSON = {
                                            response : 
                                                {
                                                status : 'Server Error',
                                                mensagemRetorno : 'Server Error'
                                                }
                                            }
                                return res.status(500).json(tokenJSON)
                            })
                    }); 
                } else {
                    tokenJSON = {
                        response :
                            {
                            status : 'Error',
                            mensagemRetorno : 'Acesso Expirado, faça login novamente'
                            }
                        }
                    return res.json(tokenJSON);
                }
            } else {
                tokenJSON = {
                        response :
                            {
                            status : 'Error',
                            mensagemRetorno : 'Token Invalido'
                            }
                        }
                return res.json(tokenJSON);
            }
        } else{
            tokenJSON = {
                        response : 
                            {
                            status : 'Error',
                            mensagemRetorno : 'Authentication Failed'
                            }
                        }
          return res.status(400).json(tokenJSON);
        }
    },

    cardcustomerfind : function(application, req, res){

        token = application.config.auth.getTokenFromHeader(req.headers);

        if (token != null) {

            decoded = application.config.auth.tokenDecode(token);

            if (decoded != null) {

                isTokenValid = application.config.auth.isTokenValid(decoded);

                if (isTokenValid) {

                    CreditDAO = application.app.models.creditDAO;

                    CreditDAO.findAll({
                        where : {
                            cpf : decoded.iss
                        }
                    })
                    .then(card => {
                        if(card[0].dataValues.cardNumber != null){

                                tokenJSON = {
                                        response :
                                            {
                                                status : 'OK',
                                                hasAccount : true,
                                                customer : {
                                                  cpf : card[0].dataValues.cpf
                                                },
                                                creditCard : {
                                                    cardHolderName : card[0].dataValues.cardholder,
                                                    maskedNumber : card[0].dataValues.cardNumber,
                                                    cardExpiry : card[0].dataValues.cardExpiry,
                                                    cardCvv : card[0].dataValues.cardCvv,
                                                    street : card[0].dataValues.street,
                                                    district : card[0].dataValues.district,
                                                    city : card[0].dataValues.city,
                                                    state : card[0].dataValues.state,
                                                    complement : card[0].dataValues.complement,
                                                    postalCode : card[0].dataValues.postalCode,
                                                    addressNumber : card[0].dataValues.addressNumber
                                                }
                                            }
                                        }
                                res.json(tokenJSON);
                        } else{
                            tokenJSON = {
                                    response :
                                        {
                                        status : 'OK',
                                        hasAccount : false
                                        }
                                    }
                                return res.json(tokenJSON);
                        }
                    })
                    .catch(err => {
                        tokenJSON = {
                                    response : 
                                        {
                                        status : 'Server Error',
                                        mensagemRetorno : 'Server Error'
                                        }
                                    }
                    return res.status(500).json(tokenJSON)
                    })
                    
                } else {
                    tokenJSON = {
                        response :
                            {
                            status : 'Error',
                            mensagemRetorno : 'Acesso Expirado, faça login novamente'
                            }
                        }
                    return res.status(400).json(tokenJSON);
                }
            } else {
                tokenJSON = {
                        response :
                            {
                            status : 'Error',
                            mensagemRetorno : 'Token Invalido'
                            }
                        }
                return res.status(400).json(tokenJSON);
            }
        } else{
            tokenJSON = {
                        response : 
                            {
                            status : 'Error',
                            mensagemRetorno : 'Authentication Failed'
                            }
                        }
          return res.status(400).json(tokenJSON);
        }
    }
}