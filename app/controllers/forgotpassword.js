var encrypt = require('./aescrypt.js');

module.exports.recuperarSenha = function(application, request, response){
	request.check('email', 'Campo email obrigatorio').notEmpty()

	validate = request.validationErrors();

	if (validate) {
		tokenJSON = {
						response : {
							status : "Error",
							retorno : validate[0].msg
						}
					}
		return response.json(tokenJSON);
		
	}

	var userDAO = application.app.models.userDAO;

	userDAO.findOne({
		where : {
			email : request.body.email
		}
	})
	.then( user => {
		if (user != null || user != undefined) {

			var text = 'Sua senha de login é '+encrypt.desencode(user.dataValues.senha) +'. Aproveite :)';
			var subject = 'Recuperar Senha';
			var emailReceivers = request.body.email;

			var emailSuccessful = application.config.email.sendEmail(text, subject, emailReceivers);

			if (emailSuccessful) {
				var tokenJson = {
	                      response : 
	                        {
	                          status : 'OK',
	                          mensagemRetorno: 'Senha enviada para o e-mail informado.'
	                        }
	                      }
	      		return response.json(tokenJson)  
			} 

			var tokenJson = {
	                      response : 
	                        {
	                          status : 'Error',
	                          mensagemRetorno: 'Houve uma falha ao enviar o e-mail'
	                        }
	                      }
	      	return response.json(tokenJson)
		} 

		var tokenJson = {
	                    response : 
	                    	{
	                          status : 'Error',
	                          mensagemRetorno: 'Nenhuma conta vinculada a esse e-mail.'
	                        }
	                    }
	      return response.json(tokenJson)

	})
	.catch(err => {
		var tokenJson = {
                        response : {
                          status : 'Server Error',
                          mensagemRetorno :'Server Error'
                        }
                      }

      	return response.status(500).json(tokenJson)
	})
}