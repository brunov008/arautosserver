var tokenJSON, validate, client; 
var encrypt = require('./aescrypt.js');

module.exports.cadastroIncluir = function(application, request, response){

  request.check('nome', 'Campo nome obrigatório').notEmpty();
  request.check('senha', 'Campo senha obrigatório').notEmpty();
  request.check('email', 'Campo email obrigatório').notEmpty();
  request.check('cpf', 'Campo cpf obrigatório').notEmpty();
  request.check('telefone', 'Campo telefone obrigatório').notEmpty();

  validate = request.validationErrors();

    if (validate) {
    	
			tokenJSON = {
        status : 'OK',
			  mensagemRetorno: validate[0]['msg']
      } 
				
			return response.json(tokenJSON);
     }
  
    var UserDAO = application.app.models.userDAO;

    UserDAO.sync()
    .then(() => {
      
      return UserDAO.create({
        nome : request.body.nome,
        senha : request.body.senha,
        email : request.body.email,
        cpf : request.body.cpf,
        telefone : request.body.telefone
      })
      .then(() => {
        tokenJSON = {
                  response : 
                    {
                    status : 'OK',
                    mensagemRetorno: 'Cadastro Efetuado com Sucesso'
                    }
                  }
        
        return response.json(tokenJSON);
      });
    })
    .catch(err => {
      if (err.parent.code == 23505) {
        tokenJSON = {
                      response : 
                        {
                        status : 'OK',
                        mensagemRetorno : 'Conta já existe no Registro'
                        }
                      }

        return response.json(tokenJSON)
      }
      tokenJSON = {
                    response : 
                      {
                      status : 'Server Error',
                      mensagemRetorno : 'Server Error'
                      }
                    }

      return response.status(500).json(tokenJSON);
    });
}
