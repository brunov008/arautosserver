var tokenJSON, validate, client, token, decoded, isTokenValid, cliente, LocalDAO;

module.exports.listLocal = function(application, request, response){
	
	token = application.config.auth.getTokenFromHeader(request.headers);

    if (token != null) {

    	decoded = application.config.auth.tokenDecode(token);

    	if (decoded != null) {
    		
    		isTokenValid = application.config.auth.isTokenValid(decoded);

    		if (isTokenValid) {

    			LocalDAO = application.app.models.callsDAO;

    			LocalDAO.findAll({
    				where : {
    					cpf : decoded.iss
    				}
    			})
    			.then(chamados => {
    				tokenJSON = {
			           	        response : 
			           	        	{
			                        status : 'OK',
			                        mensagemRetorno : 'Lista gerada com sucesso',
			                        retorno : chamados
			                       	}
			                    }
			        return response.json(tokenJSON);
			    })
    			.catch(err => {
    				tokenJSON = {
								response:
									{
									status : "Server Error",
									retorno : "Server Error"
									}
								}
					return response.status(500).json(tokenJSON);
    			})
    		} else{
    			tokenJSON = {
						response :
							{
	                    	status : 'Error',
	                        mensagemRetorno : 'Acesso Expirado, faça login novamente'
	                    	}
	                    }
    			return response.json(tokenJSON);
    		}
    	} else {
    		tokenJSON = {
						response :
							{
	                    	status : 'Error',
	                        retorno : 'Token Invalido'
	                    	}
	                    }
	        return response.status(400).json(tokenJSON);
    	}	
	} else {
		 	tokenJSON = {
						response : 
							{
	                    	status : 'Error',
	                        retorno : 'Authentication Failed'
	                    	}
	                    }
	      return response.status(400).json(tokenJSON);
	 }	
}

module.exports.insertLocal = function(application, request, response){
	request.check('location', 'Campo location obrigatorio').notEmpty();
	request.check('longitude', 'Campo longitude obrigatorio').notEmpty();
	request.check('latitude', 'Campo latitude obrigatorio').notEmpty();
	request.check('date', 'Campo date obrigatorio').notEmpty();
	request.check('hour', 'Campo hour obrigatorio').notEmpty();

	validate = request.validationErrors();

	if (validate) {
		tokenJSON = {
						response : {
							status : "Error",
							retorno : validate[0].msg
						}
					}
		return response.json(tokenJSON);
		
	}
	
	token = application.config.auth.getTokenFromHeader(request.headers);

	if (token != null) {

		decoded = application.config.auth.tokenDecode(token);

		if (decoded != null) {

			isTokenValid = application.config.auth.isTokenValid(decoded);

			if (isTokenValid) {

				LocalDAO = application.app.models.callsDAO;

				LocalDAO.sync()
				.then(() => {
					return LocalDAO.create({
						cpf : decoded.iss,
						location : request.body.location,
						date : request.body.date,
						hour : request.body.hour,
						longitude : request.body.longitude,
						latitude : request.body.latitude
					})
				})
				.then(() => {

					UserDAO = application.app.models.callsDAO;

					UserDAO.findAll({
						where : {
							cpf : decoded.iss
						}
					})
					.then(user => {
						var recordedLocal = {
							location : request.body.location,
							date : request.body.date,
							hour : request.body.hour,
							longitude : request.body.longitude,
							latitude : request.body.latitude
						}

						var text = 'Local : '+request.body.location + "\n" + "Data : "+request.body.date +"\n" + "Hora : "+request.body.hour + "\n" + "Nome : " + user[0].dataValues.nome + "\n" + "Celular : " + user[0].dataValues.telefone
						var subject = 'Local do evento';
						var mailList = 'brunomarcio2006@gmail.com, bruno_correa_20@hotmail.com';

						// var emailSuccessful = application.config.email.sendEmail(text, subject, mailList);

						// if (emailSuccessful) {
							tokenJSON = {
				           	       response : 
				           	       {
									status : 'OK',
									mensagemRetorno: 'OK'
									}
								}
			        		return response.json(tokenJSON);
						// }

						/*tokenJSON = {
				           	       response : 
				           	       {
									status : 'Error',
									mensagemRetorno: 'Ocorreu um erro inesperado, tente mais tarde'
									}
								}
			        	return response.json(tokenJSON);*/
					})
					.catch(err => {
						tokenJSON = {
								response:
									{
									status : "Server Error",
									retorno : err
									}
								}
						return response.status(500).json(tokenJSON);
					})

				})
				.catch(err => {
					tokenJSON = {
								response:
									{
									status : "Server Error",
									retorno : err
									}
								}
					return response.status(500).json(tokenJSON);
				})
			} else{
				tokenJSON = {
						response :
							{
	                    	status : 'Error',
	                        mensagemRetorno : 'Acesso Expirado, faça login novamente'
	                    	}
	                    }
    			return response.json(tokenJSON);
			}

		} else{
			tokenJSON = {
						response :
							{
	                    	status : 'Error',
	                        retorno : 'Token Invalido'
	                    	}
	                    }
	        return response.status(400).json(tokenJSON);
		}

	} else{
		tokenJSON = {
					response : 
						{
	                   	status : 'Error',
	                    retorno : 'Authentication Failed'
	                   	}	
	                }
	    return response.status(400).json(tokenJSON);
	}
}

module.exports.deleteLocal = function(application, request, response){
	request.check('latitude', 'Campo Obrigatorio').notEmpty();
	request.check('longitude', 'Campo Obrigatorio').notEmpty();
	request.check('date', 'Campo Obrigatorio').notEmpty();
	request.check('hour', 'Campo Obrigatorio').notEmpty();

	validate = request.validationErrors();

	if (validate) {
		tokenJSON = {
					response:
						{
							status : 'Error',
							retorno : validate[0].msg
						}
					}
		return response.status(400).json(tokenJSON);
	};

	token = application.config.auth.getTokenFromHeader(request.headers);

	if (token != null) {
		decoded = application.config.auth.tokenDecode(token);

		if (decoded != null) {
			isTokenValid = application.config.auth.isTokenValid(decoded);

			if (isTokenValid) {

				LocalDAO = application.app.models.callsDAO;

				LocalDAO.findAll({
					where : {
						longitude : request.body.longitude
					}
				})
				.then(chamados => {
					return chamados.destroy();
				})
				.then(() => {
					response.json('ok')
				})
				.catch(err => {
					response.json('error')
				})
			} else{
				tokenJSON = {
						response :
							{
	                    	status : 'Error',
	                        mensagemRetorno : 'Acesso Expirado, faça login novamente'
	                    	}
	                    }
    			return response.status(400).json(tokenJSON);
			}

		} else{
			tokenJSON = {
						response :
							{
	                    	status : 'Error',
	                        retorno : 'Token Invalido'
	                    	}
	                    }
	        return response.status(400).json(tokenJSON);
		}
	} else{
		tokenJSON = {
					response : 
						{
	                   	status : 'Error',
	                    retorno : 'Authentication Failed'
	                   	}	
	                }
	      return response.status(400).json(tokenJSON);
	}
}