module.exports.login = function(application, request, response){

 request.check('cpf', 'Campo cpf obrigatorio').notEmpty()
 request.check('senha', 'Campo senha obrigatorio').notEmpty()

 var validate = request.validationErrors();

    if (validate) {
    	var tokenJson = {
                          response : {
                            status : 'Ok',
                            retorno : validate[0]['msg']
                          }
                        }
				
				return response.json(tokenJson)
    }

    var UserDAO = application.app.models.userDAO

    UserDAO.findAll({
      where : {
        cpf : request.body.cpf,
        senha : request.body.senha
      }
    })
    .then(user => {
      if (user[0] != undefined) {

        var token = application.config.auth.tokengenerate(user[0].dataValues.cpf)
            
        var tokenJson = {
                        response : 
                         {
                          status : 'OK',
                          retorno : 'Consulta efetuada com sucesso',
                          token : token,
                          user: {
                            nome : user[0].dataValues.nome,
                            telefone : user[0].dataValues.telefone,
                            email : user[0].dataValues.email,
                            cpf : user[0].dataValues.cpf,
                            nonce : user[0].dataValues.nonce,
                            customerId : user[0].dataValues.customerId
                          }
                         }
                       }   
        return response.json(tokenJson);
      }
      var tokenJson = {
                      response : 
                        {
                          status : 'OK',
                          retorno: 'Usuario não encontrado'
                        }
                      }
      return response.json(tokenJson)      

    })
    .catch(err => {
      var tokenJson = {
                        response : {
                          status : 'Server Error',
                          retorno :'Server Error'
                        }
                      }

      return response.status(500).json(tokenJson)
    })
}