var crypto = require('crypto');

var key = crypto.createHash("sha256").update("Nexnogen1").digest();
var iv = '4e5Wa71fYoT7MFEX';

module.exports = {

	encode : function(text){

    	var encipher = crypto.createCipheriv('aes-256-cbc', key, iv);
        return Buffer.concat([
            encipher.update(text),
            encipher.final()
        ]).toString('base64');
	},

	desencode : function(encryptdata){

    	var decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);
        return Buffer.concat([
            decipher.update(encryptdata, 'base64'), // Expect `text` to be a base64 string
            decipher.final()
          ]).toString()
    }
};