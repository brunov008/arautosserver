var Sequelize = require('sequelize');
var sequelize = require('../../config/connectiondb').pgConnect();

module.exports = function(){
	
	var options = {
    	timestamps : false,
    	paranoid : true,
    	underscored: true,
    	freezeTableName: true,
    	tableName: 'user'
    }

    return sequelize.define('user', {
    	id : {
				type : Sequelize.INTEGER,
				primaryKey : true,
				autoIncrement : true
		},
		
		nome : {
			type : Sequelize.STRING,
			allowNull : false
		},
		
		senha : {
			type : Sequelize.STRING,
			allowNull : false
		},
		
		email : {
			type : Sequelize.STRING,
			allowNull : false
		},
		
		telefone: {
			type : Sequelize.STRING,
			allowNull : false
		},
		
		cpf : {
			type : Sequelize.STRING,
			unique : true,
			allowNull : false
		}
    }, options);
};
