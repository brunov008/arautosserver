var Sequelize = require('sequelize');
var sequelize = require('../../config/connectiondb').pgConnect();

module.exports = function(){
	
	var options = {
    	timestamps : false,
    	paranoid : false,
    	underscored: true,
    	freezeTableName: true,
    	tableName: 'chamados'
    }

    return sequelize.define('chamados', {
    	idChamado : {
				type : Sequelize.INTEGER,
				primaryKey : true,
				autoIncrement : true
		},
		cpf : {
			type : Sequelize.STRING,
			allowNull : false
		},
		location : {
			type : Sequelize.STRING,
			allowNull : false
		},
		date : {
			type : Sequelize.STRING,
			allowNull : false
		},
		hour : {
			type : Sequelize.STRING,
			allowNull : false
		},

		longitude : {
			type : Sequelize.STRING,
			allowNull : false
		},
		
		latitude : {
			type : Sequelize.STRING,
			allowNull : false
		}
    }, options);
}