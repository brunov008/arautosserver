var Sequelize = require('sequelize');
var sequelize = require('../../config/connectiondb').pgConnect();

module.exports = function(){
	
	var options = {
    	timestamps : false,
    	paranoid : true,
    	underscored: true,
    	freezeTableName: true,
    	tableName: 'card'
    }


    return sequelize.define('card', {
    	idCard : {
			type : Sequelize.INTEGER,
			primaryKey : true,
			autoIncrement : true
		},

		cpf : {
			type : Sequelize.STRING,
			allowNull : false
		},

		cardholder : {
			type : Sequelize.STRING,
			allowNull : false
		},

		cardNumber : {
			type : Sequelize.STRING,
			allowNull : false
		},

		cardExpiry : {
			type : Sequelize.STRING,
			allowNull : false
		},

		postalCode : {
			type : Sequelize.STRING,
			allowNull : false
		},

		cardCvv : {
			type : Sequelize.STRING,
			allowNull : false
		},

		street : {
			type : Sequelize.STRING,
			allowNull : false
		},

		district : {
			type : Sequelize.STRING,
			allowNull : false
		},

		city : {
			type : Sequelize.STRING,
			allowNull : false
		},

		state : {
			type : Sequelize.STRING,
			allowNull : false
		},

		complement : {
			type : Sequelize.STRING,
			allowNull : true
		},

		addressNumber : {
			type : Sequelize.STRING,
			allowNull : true
		}
    })
}