module.exports = function(application){
	application.post('/recuperar/senha', function(request, response){
		application.app.controllers.forgotpassword.recuperarSenha(application, request, response);
	});
}