module.exports = function(application){
	application.get('/local/cadastrado', function(request, response){
		application.app.controllers.local.listLocal(application, request, response);
	});

	application.put('/local/inserir', function(request, response){
		application.app.controllers.local.insertLocal(application, request, response);
	});

	application.delete('/local/excluir', function(request, response){
		application.app.controllers.local.deleteLocal(application, request, response);
	});
}