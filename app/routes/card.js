module.exports = function(application){
	application.get('/cartao/token', function(request, response){
		application.app.controllers.card.cardtoken(application, request, response);
	});

	application.post('/cartao/createcustomer', function(request, response){
		application.app.controllers.card.cardcustomercreate(application, request, response);
	});

	application.get('/cartao/cardcustomerfind', function(request, response){
		application.app.controllers.card.cardcustomerfind(application, request, response);
	});
}